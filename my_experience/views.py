from django.shortcuts import render
from django.http import HttpResponse

# Create your views here.
def my_experience(request):
    return render(request,'my_experience.html')

def my_education(request):
    return render(request,'my_education.html')