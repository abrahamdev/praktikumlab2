from django.shortcuts import render
from datetime import datetime, date
from django.http import HttpResponseRedirect
from .forms import Message_Form
from .models import Message


# Enter your name here
mhs_name = 'Devi Abraham Syamsuardi' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1985,6,22) #TODO Implement this, format (Year, Month, Date)
npm = None # TODO Implement this
asal_drh = 'Bandung'
work = 'Karyawan Swasta'
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'asal': asal_drh, 'kerja': work, 'tgl_lhr':birth_date}
    return render(request, 'index_lab1.html', response)
    response['message_form'] = Message_Form
    return render(request, html, response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0

#def index(request):
#    response['message_form'] = Message_Form
#    return render(request, html, response)

def message_post(request):
    response={}
    form = Message_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
        response['email'] = request.POST['email'] if request.POST['email'] != "" else "Anonymous"
        response['message'] = request.POST['message']
        message = Message(name=response['name'], email=response['email'], message=response['message'])
        message.save()
        html ='[nama_appsmu]/form_result.html'
        return render(request, html, response)
    else:
        return HttpResponseRedirect('/')

def message_table(request):
    response = {}
    message = Message.objects.all()
    response['message'] = message
    html = 'templates/table.html'
    return render(request, html, response)

